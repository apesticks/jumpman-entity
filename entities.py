from library import WorldEntity
from data import Position, Velocity, Jump, Data, Control, Bindings
from library import colors
import pdb

class Player(WorldEntity):
    def __init__(self, name, position=(0, 0), size=(0, 0)):
        self.data = Data("Player")
        self.sprite = self.factory.from_color(colors.BLUE, size=size)
        self.position = Position(*position)
        self.velocity = Velocity()
        self.jump = Jump()
        self.bindings = Bindings()

class Platform(WorldEntity):
    def __init__(self, name, position=(0, 0), size=(0, 0)):
        self.data = Data(name)
        self.sprite = self.factory.from_color(colors.GRAY, size=size)
        self.position = Position(*position)
        #self.sprite.position = self.position.x, self.position.y

class Controls(WorldEntity):
    def __init__(self):
        self.control = Control()

class DebugBindings(WorldEntity):
    def __init__(self):
        self.bindings = Bindings()
