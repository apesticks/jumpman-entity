from __future__ import division
from sdl2 import SDL_GetTicks, SDL_Delay
from sdl2.ext import Entity
import sdl2.ext as sdl
import registry

def debug(*msg):
    if registry.DEBUG == True:
        print("[DEBUG]: " + ", ".join([str(item) for item in msg]))

# A direct port of clock_tick_base from pygame's time.c
class Clock(object):
    def __init__(self):
        self.fps_tick = None
        self.last_tick = SDL_GetTicks()
        self.fps_count = 0

        self.rawpassed = 0

    def tick(self, framerate):
        framerate = float(framerate)
        endtime = int(((1.0 / framerate) * 1000.0))
        self.rawpassed = SDL_GetTicks() - self.last_tick
        delay = endtime - self.rawpassed

        if delay < 0:
            delay = 0

        # execute the actual delay
        SDL_Delay(delay)

        nowtime = SDL_GetTicks()

        self.timepassed = nowtime - self.last_tick;
        self.fps_count += 1
        self.last_tick = nowtime

        if self.fps_tick == None:
            self.fps_tick = nowtime
        elif self.fps_count >= 10:
            self.fps = self.fps_count / (float(nowtime - self.fps_tick) / 1000)
            self.fps_count = 0
            self.fps_tick = nowtime

        return int(self.timepassed)

class WorldEntity(Entity):
    """ Assuming a global world, avoid the need to pass in world repeatedly"""
    def __new__(cls, *args, **kwargs):
        cls.factory = registry.factory
        return Entity.__new__(cls, registry.world, *args, **kwargs)

# Pseudo-class to store color. Use from library import colors
class colors(object):
    BLUE = sdl.Color(0, 0, 255)
    BLACK = sdl.Color(0, 0, 0)
    GRAY = sdl.Color(211,211,211)
