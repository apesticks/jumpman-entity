import sdl2.ext as sdl
from sdl2 import *
import registry, sys, pdb
from library import Clock

def run():
    clock = Clock()
    registry.window.show()

    while registry.running:
        clock.tick(60)
        registry.world.process()

if __name__ == "__main__":
    sys.exit(run())
