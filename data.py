import sdl2.ext, pdb
import sdl2.keycode as keycodes

class Position(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

class Velocity(object):
    def __init__(self, initial_speed=0, max_speed=7):
        self.max_speed = max_speed
        self.speed = initial_speed

class Jump(object):
    def __init__(self, initial_accend=0, max_accend=10):
        self.accend = initial_accend
        self.max_accend = max_accend
        self.LANDED = self.PROCESS = False

class Bindings(object):
    def __init__(self):
        self.UP = False
        self.RIGHT = False
        self.LEFT = False
        self.r = False
        self.t = False

class Data(object):
    def __init__(self, name, is_AI=False):
        self.name = name
        self.is_AI = is_AI

class Control(object):
    def __init__(self):
        self.key_names = []

        for key in keycodes.__dict__:
            if "SDLK" in key:
                self.key_names.append(key)
                key_name = key.split("SDLK_")[1]
                setattr(self, key_name, False)
