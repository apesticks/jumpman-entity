import sdl2.ext as sdl
from systems import RenderTexture, MovementSystem, JumpSystem, CollisionSystem, \
					PositionSystem, FreefallSystem, EventSystem, KeyBindings, DebugSystem
from entities import Player, Platform, Controls

sdl.init()

#globals
running = True
DEBUG = True
WINDOW_SIZE = 800, 600
PLAYER_SIZE = 30, 30
FLOOR_SIZE = 800,10
TOP_MIDDLE = WINDOW_SIZE[0]/2-(PLAYER_SIZE[0]/2), PLAYER_SIZE[0]
BOTTOM_MIDDLE = WINDOW_SIZE[0]/2-(PLAYER_SIZE[0]/2), WINDOW_SIZE[1]-PLAYER_SIZE[0]-FLOOR_SIZE[1]

#sdl variables
world = sdl.World()
window = sdl.Window("SS Engine", size=WINDOW_SIZE)
render = sdl.RenderContext(window)
factory = sdl.SpriteFactory(sdl.TEXTURE, renderer=render)

#entities
controls = Controls()
player = Player("player", position=(WINDOW_SIZE[0]/2-(PLAYER_SIZE[0]/2), 100), size=PLAYER_SIZE)
floor = Platform("floor", position=(0, 300), size=FLOOR_SIZE)
wall1 = Platform("wall1", position=(200, 200), size=(20, 100))
wall2 = Platform("wall2", position=(500, 190), size=(20, 100))
wall3 = Platform("wall3", position=(300, 260), size=(50, 20))
wall4 = Platform("wall4", position=(400, 220), size=(50, 20))
wall5 = Platform("wall5", position=(250, 220), size=(50, 20))

#init systems
systems = [
	RenderTexture(render),
	MovementSystem(),
	JumpSystem(),
	CollisionSystem(player),
	PositionSystem(),
	FreefallSystem(player),
	EventSystem(controls),
	KeyBindings(controls),
	DebugSystem(),
]

#world
for system in systems:
	world.add_system(system)
