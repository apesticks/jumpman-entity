import sdl2.ext as sdl

class MainMenuSystem(sdl.Applicator):
    def __init__(self, player):
        super(MainMenuSystem, self).__init__()
        self.componenttypes = (Control,)

    def control_setup(self, control):
    	pass

    def process(self, world, entities):
    	for control in entities:
    		self.control_setup(control)



#System that had all the key bindings, meaning it had access to anything that had keybindings and updated them in that system