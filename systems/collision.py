import sdl2.ext as sdl
from data import Position, Data

class CollisionSystem(sdl.Applicator):
    def __init__(self, player):
        super(CollisionSystem, self).__init__()
        self.componenttypes = (Data, Position, sdl.Sprite)
        self.player = player

    def check_overlap(self, sprite, data):
        if data.name == "Player": return False

        speed = self.player.velocity.speed
        accend = self.player.jump.accend
        s_left, s_top, s_right, s_bottom = sprite.area
        p_left, p_top, p_right, p_bottom = self.player.sprite.area
        
        return (p_left + speed) < s_right and (p_right + speed) > s_left \
               and (p_top + accend) < s_bottom and p_bottom > (s_top - accend) \

    def set_collision_direction(self, sprite):
        s_left, s_top, s_right, s_bottom = sprite.area
        p_left, p_top, p_right, p_bottom = self.player.sprite.area

        direction_list = {
            abs(s_left-p_right): "right",
            abs(s_right-p_left): "left",
            abs(s_top-p_bottom): "bottom",
            abs(s_bottom-p_top): "top",
        }

        direction_index = min(abs(s_left-p_right), abs(s_right-p_left), abs(s_top-p_bottom), abs(s_bottom-p_top))
        return direction_list[direction_index]

    def handle_collisions(self, sprite, collision_direction):
        #wat
        if collision_direction == "bottom":
            if self.player.jump.accend > 0:
                self.player.jump.LANDED = True
                self.player.position.y = sprite.area[1] - self.player.sprite.size[0]

        if collision_direction == "top":
            if self.player.jump.accend < 0:
                self.player.jump.max_accend = 0
                self.player.position.y = sprite.area[3]

        if collision_direction == "left":
            if self.player.velocity.speed < 0:
                self.player.position.x = sprite.area[2]
                self.player.velocity.speed = 0

        if collision_direction == "right":
            if self.player.velocity.speed > 0:
                self.player.position.x = sprite.area[0] - self.player.sprite.size[0]
                self.player.velocity.speed = 0

    def process(self, world, component_sets):
        for data, position, sprite in component_sets:
            collision_direction = None
            if self.check_overlap(sprite, data):
                collision_direction = self.set_collision_direction(sprite)
                self.handle_collisions(sprite, collision_direction)

