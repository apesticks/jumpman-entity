import sdl2.ext as sdl
from data import Position

class PositionSystem(sdl.Applicator):
    def __init__(self):
        super(PositionSystem, self).__init__()
        self.componenttypes = (Position, sdl.Sprite)

    def process(self, world, component_sets):
        for position, sprite in component_sets:
            sprite.position = position.x, position.y
