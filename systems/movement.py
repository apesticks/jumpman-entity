import sdl2.ext as sdl
from data import Position, Velocity, Data, Bindings

class MovementSystem(sdl.Applicator):
    def __init__(self):
        super(MovementSystem, self).__init__()
        self.componenttypes = (Bindings, Velocity, Position)
        self.delay_amount = 10

    def acceleration(self, b, v):
        if abs(v.speed) <= v.max_speed:
            if b.RIGHT: v.speed += 1
            if b.LEFT: v.speed -= 1

        if abs(v.speed) > v.max_speed:
            if v.speed > 0: v.speed -= 1
            if v.speed < 0: v.speed += 1

        if (not b.RIGHT and not b.LEFT) or (b.RIGHT and b.LEFT):
            if v.speed != 0:
                if v.speed < 0: v.speed += 1
                if v.speed > 0: v.speed -= 1

    def process(self, world, components_sets):
        for bindings, velocity, position, in components_sets:
            self.acceleration(bindings, velocity)
            position.x += velocity.speed
