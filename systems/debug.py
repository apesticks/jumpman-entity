import sdl2.ext as sdl
from data import Bindings
import registry, pdb

class DebugSystem(sdl.Applicator):
    def __init__(self):
        super(DebugSystem, self).__init__()
        self.componenttypes = (Bindings,)

    def debug_bindings(self, bindings):
    	if bindings.r:
    		registry.player.position.x, registry.player.position.y = registry.TOP_MIDDLE
    		registry.player.jump.accend = 0
    		registry.player.jump.max_accend = 0
        if bindings.t:
        	pdb.set_trace()

    def process(self, world, component_sets):
    	for bindings in component_sets:
    		self.debug_bindings(bindings[0])

