import sdl2.ext as sdl
from sdl2 import *
from data import Control
import pdb

class EventSystem(sdl.Applicator):
    def __init__(self, controls):
        super(EventSystem, self).__init__()
        self.componenttypes = (Control,)
        self.keyboard_dict = {}
        self.controls = controls

    def set_key_dict(self, controls):
        for key, value in keycode.__dict__.items():
            if str(value).isdigit() and "SDLK_" in key:
                new_key = key.split("SDLK_")[1]
                self.keyboard_dict[value] = new_key

    def process(self, world, entities):
        if self.keyboard_dict == {}:
           self.set_key_dict(self.controls)

        events = sdl.get_events()
        for event in events:    
            key = event.key.keysym.sym

            if event.type == SDL_KEYDOWN:
                key_name = self.keyboard_dict[key]
                self.controls.control.__dict__[key_name] = True

            if event.type == SDL_KEYUP:
                key_name = self.keyboard_dict[key]
                self.controls.control.__dict__[key_name] = False

