import sdl2.ext as sdl
import registry
from library import colors

# no longer used, keep as backup incase we need to switch back to sw rendering
class RenderSoftware(sdl.SoftwareSpriteRenderer):
    def __init__(self, window):
        super(RenderSoftware, self).__init__(window)

    def render(self, components):
        sdl.fill(self.surface, colors.BLACK)
        super(RenderSoftware, self).render(components)

class RenderTexture(sdl.TextureSpriteRenderer):
    def __init__(self, renderer):
        super(RenderTexture, self).__init__(renderer)
        self.renderer = renderer

    def render(self, components):
        tmp = self.renderer.color
        self.renderer.color = colors.BLACK
        self.renderer.clear()
        self.renderer.color = tmp
        super(RenderTexture, self).render(components)
