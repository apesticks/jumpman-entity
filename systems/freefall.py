import sdl2.ext as sdl
import pdb
from data import Position, Jump, Data

class FreefallSystem(sdl.Applicator):
    def __init__(self, player):
        super(FreefallSystem, self).__init__()
        self.componenttypes = (Data, Position, sdl.Sprite)
        self.player = player
        self.platform_name = None
        self.all_colliding_objects = []

    def freefall_possible(self, s, d):
        if self.platform_name == None: return True
        if d.name != self.platform_name: return False
        if not self.objects_colliding(s): return True
        return False

    def set_platform_name(self, s, d):
        if self.objects_colliding(s) and d.name not in self.all_colliding_objects:
            self.platform_name = d.name
            self.all_colliding_objects.append(self.platform_name)

    def objects_colliding(self, s):
        p_range_x = range(self.player.sprite.area[0], self.player.sprite.area[2])
        s_range_x = range(s.area[0], s.area[2])
        
        if self.player.sprite.area[3] == s.area[1]:
            for x in p_range_x:
                if x in s_range_x:
                    return True

    def reset_platform_name(self):
        if len(self.all_colliding_objects) == 0: 
            self.platform_name = None
        self.all_colliding_objects = []

    def set_freefall_conditions(self):
        self.player.jump.LANDED = False
        self.player.jump.PROCESS = True
        self.player.jump.max_accend = -1

    def process(self, world, component_sets):
        for data, position, sprite in component_sets:
            self.set_platform_name(sprite, data)
            if self.freefall_possible(sprite, data) and not self.player.jump.PROCESS:
                self.set_freefall_conditions()
        self.reset_platform_name()








