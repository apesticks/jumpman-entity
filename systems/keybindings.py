import sdl2.ext as sdl
from data import Bindings
import pdb
class KeyBindings(sdl.Applicator):
    def __init__(self, controls):
        super(KeyBindings, self).__init__()
        self.componenttypes = (Bindings,)
        self.control = controls.control
    
    def bind_keys(self, bindings):
        bindings.UP = self.control.UP
    	bindings.RIGHT = self.control.RIGHT
    	bindings.LEFT = self.control.LEFT
        bindings.r = self.control.r
        bindings.t = self.control.t

    def rebind(self):
    	pass

   	def unbind(self):
   		pass

   	def multi_bind(self):
   		pass

    def process(self, world, component_sets):
    	for bindings in component_sets:
    		self.bind_keys(bindings[0])

