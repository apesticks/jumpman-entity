import sdl2.ext as sdl
from data import Position, Jump, Control, Bindings

class JumpSystem(sdl.Applicator):
    def __init__(self):
        super(JumpSystem, self).__init__()
        self.componenttypes = (Bindings, Jump, Position)


    def jump_process(self, b, j):
        if b.UP: j.PROCESS = True

        if j.PROCESS and not j.LANDED:
            j.accend = -j.max_accend
            j.max_accend -= 1

        if j.LANDED:
            j.max_accend = 10
            j.accend = 0
            j.PROCESS = False
        
        if j.LANDED and not b.UP: 
            j.LANDED = False

    def process(self, world, component_sets):
        for bindings, jump, position, in component_sets:
            self.jump_process(bindings, jump)
            position.y += jump.accend

